# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-libs/swl/swl-0.4.0_rc5-r2.ebuild,v 1.2 2005/08/03 01:38:36 trapni Exp $

inherit flag-o-matic multilib

DESCRIPTION="mod_transform plugins for communicating with a YaCS server"
HOMEPAGE="http://battousak.mylair.de/yacs/"
SRC_URI="http://battousai.mylair.de/dist/yacs/yacs-${PV}.tar.bz2"
LICENSE="LGPL-2.1"
SLOT="0.6"
KEYWORDS="~amd64 ~x86"
IUSE="debug multislot"

LAST_PV="0.6.0_pre20070208"
RDEPEND=">=www-apache/mod_transform-0.7.0_pre20070204
		 >=dev-libs/libyacsclient-${LAST_PV}
		 >=dev-libs/libyacsutil-${LAST_PV}"

DEPEND="${RDEPEND}"

BASE_DIR=${BASE_DIR:-/usr}

S="${WORKDIR}/yacs-${PV}"

src_compile() {
	use debug && append-flags -O0 -g3
	use debug || append-flags -DNDEBUG=1

	useq multislot && [[ $BASE_DIR = "/usr" ]] && BASE_DIR="${BASE_DIR}/yacs/${SLOT}"

	einfo "Configuring for prefix ${BASE_DIR} ..."

	./autogen.sh || die "autogen.sh failed"

	./configure \
		--prefix="${BASE_DIR}" \
		--host="${CHOST}" \
		--libdir="${BASE_DIR}/$(get_libdir)" \
		--without-libyacsutil \
		--disable-server \
		--without-libyacsclient \
		|| die "./configure failed"

	cd clients/${PN/yacs/plugins} || die

	emake || die "make failed"
}

src_install() {
	cd clients/${PN/yacs/plugins} || die

	make install DESTDIR="${D}" || die

	# cleanup unwanted files from plugin/module dirs
	find ${D}/usr/lib/apache2/modules/mod_transform \( -name '*.a' -o -name '*.la' \) -exec rm -f {} \;

#	dodoc AUTHORS ChangeLog* NEWS README* TODO

	true
}

# vim:ai:noet:ts=4:nowrap
