# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-libs/swl/swl-0.5.0_pre20070122.ebuild,v 1.1 2007/01/22 07:31:48 trapni Exp $

inherit flag-o-matic multilib

DESCRIPTION="YaCS server daemon"
HOMEPAGE="https://developer.ninchens.de/yacs/"
SRC_URI="https://ninchens.de/~trapni/dist/yacs/yacs-${PV}.tar.bz2"
LICENSE="LGPL-2.1"
SLOT="0.6"
KEYWORDS="~amd64 ~x86"
IUSE="debug doc multislot character chat example news relationship"

# the last compatible version for package internal dependencies
PV_COMPAT="0.6.0_pre20070202"

# the last compatible version for the rc files being used in this ebuild
PV_rc="0.6.0_pre20070201"

RDEPEND=">=dev-libs/libyacsutil-${PV_COMPAT}
		 >=sys-libs/cracklib-2.8.9-r1"

DEPEND="${RDEPEND}
		doc? ( >=app-doc/doxygen-1.3.9.1 )"

S="${WORKDIR}/yacs-${PV}"

BASE_DIR=${BASE_DIR:-/usr}

pkg_setup() {
	enewgroup yacs 71
	enewuser yacs 71 -1 / yacs
}

src_compile() {
	use debug && append-flags -O0 -g
	use debug || append-flags -DNDEBUG=1

	useq multislot && [[ $BASE_DIR = "/usr" ]] && BASE_DIR="${BASE_DIR}/yacs/${SLOT}"

	einfo "Configuring for prefix ${BASE_DIR} ..."

	local sysconfdir="${BASE_DIR}/etc"
	useq multislot && sysconfdir="/etc/yacs-${SLOT}"

	local piddir="${BASE_DIR}/var/var"
	useq multislot && piddir="/var/run/yacs-${SLOT}"

	./configure \
		--prefix="${BASE_DIR}" \
		--host="${CHOST}" \
		--sysconfdir="${sysconfdir}" \
		--libdir="${BASE_DIR}/$(get_libdir)" \
		--with-piddir="${piddir}" \
		--without-libyacsutil \
		--without-libyacsclient \
		--with-server \
		$(use_enable character mod_character) \
		$(use_enable chat mod_chat) \
		$(use_enable relationship mod_relationship) \
		$(use_enable news mod_news) \
		$(use_enable example mod_example) \
		--disable-mod_{cms,crm,rpg} \
		--without-mod_ychatstream \
		--without-mod_transform-plugins \
		|| die "./configure failed"

	emake -C server/ || die "make for ABI ${ABI} failed"

	if use doc; then
		#ewarn "TODO: generate docs {html,man} via doxygen"
		#make -C doc api-docs
		# XXX: install example/test files?
		true
	fi
}

src_install() {
	# upstream install
	make -C server/ install DESTDIR="${D}" || die

	# multislot-dependant stuff
	local configdir piddir rcfile

	if useq multislot; then
		dodir /etc/yacs-${SLOT}
		configdir="/etc/yacs-${SLOT}"
		piddir="/var/run/yacs-${SLOT}"
		rcfile="yacsd-${SLOT}"

		keepdir /var/run/yacs-${SLOT}
		keepdir /var/log/yacs-${SLOT}
	else
		dodir /etc/yacs
		configdir="/etc/yacs"
		piddir="/var/run"
		rcfile="yacsd"

		dodir /var/run
		dodir /var/log
	fi

	# install rc files
	cp ${FILESDIR}/yacsd.rc-${PV_rc} yacsd.rc || die
	cp ${FILESDIR}/yacsd.rc.conf-${PV_rc} yacsd.confd || die

	for x in yacsd.rc yacsd.confd; do
		sed -i \
			-e "s,/usr/yacs/0.6,${BASE_DIR}," \
			-e "s,/var/run/yacs-0.6,${piddir}," \
			-e "s,/etc/yacs-0.6,${configdir}," \
			$x || die
	done

	newinitd yacsd.rc ${rcfile} || die
	newconfd yacsd.confd ${rcfile} || die

	# install yacsd config file
	local configfile="${D}/${configdir}/yacsd.conf"
	cp server/yacsd.conf-dist ${configfile} || die
	sed -i -e "s,/opt/surakware/lib,${BASE_DIR}/$(get_libdir)," ${configfile} || die
	sed -i -e 's,User = "yacsd",User = "yacs",' ${configfile} || die
	sed -i -e 's,Mail = "yacsd@MY.HOST.NET",Mail = "yacs@localhost",' ${configfile} || die

	# install common docs
	dodoc AUTHORS ChangeLog* NEWS README* TODO

	if use doc; then
		#ewarn "TODO: install man-pages and html version via doxygen"
		#dodoc -r doc/html
		true
	fi
}

# vim:ai:noet:ts=4:nowrap
