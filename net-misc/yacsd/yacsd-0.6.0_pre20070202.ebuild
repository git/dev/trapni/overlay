# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-libs/swl/swl-0.5.0_pre20070122.ebuild,v 1.1 2007/01/22 07:31:48 trapni Exp $

inherit flag-o-matic multilib

DESCRIPTION="YaCS server daemon"
HOMEPAGE="http://battousai.mylair.de/yacs/"
SRC_URI="http://battousai.mylair.de/dist/yacs/yacs-${PV}.tar.bz2"
LICENSE="LGPL-2.1"
SLOT="0.6"
KEYWORDS="~amd64 ~x86"
IUSE="debug doc multislot character chat examples news relationship"

RDEPEND=">=dev-libs/libyacsutil-${PV}
		 >=sys-libs/cracklib-2.8.9-r1"

DEPEND="${RDEPEND}
		doc? ( >=app-doc/doxygen-1.3.9.1 )"

S="${WORKDIR}/yacs-${PV}"

BASE_DIR=${BASE_DIR:-/usr}

src_compile() {
	use debug && append-flags -O0 -g3
	use debug || append-flags -DNDEBUG=1

	useq multislot && [[ $BASE_DIR = "/usr" ]] && BASE_DIR="${BASE_DIR}/yacs/${SLOT}"

	einfo "Configuring for prefix ${BASE_DIR} ..."

#	./autogen.sh || die "autogen.sh failed"

	local sysconfdir="${BASE_DIR}/etc"
	useq multislot && sysconfdir="/etc/yacs-${SLOT}"

	local piddir="${BASE_DIR}/var/var"
	useq multislot && piddir="/var/run/yacs-${SLOT}"

	./configure \
		--prefix="${BASE_DIR}" \
		--host="${CHOST}" \
		--sysconfdir="${sysconfdir}" \
		--libdir="${BASE_DIR}/$(get_libdir)" \
		--with-piddir="${piddir}" \
		--without-libyacsutil \
		--without-libyacsclient \
		--with-server \
		--without-mod_ychatstream \
		--without-mod_transform-plugins \
		|| die "./configure for ABI ${ABI} failed"

	cd server/ || die

	emake || die "make for ABI ${ABI} failed"

	cd - || die

	if use doc; then
		#ewarn "TODO: generate docs {html,man} via doxygen"
		#make -C doc api-docs
		# XXX: install example/test files?
		true
	fi
}

src_install() {
	# upstream install
	cd server/ || die

	make install DESTDIR="${D}" || die

	cd -

	# cleanup unwanted files from plugin/module dirs
	find ${D}/${BASE_DIR}/$(get_libdir)/yacs \( -name '*.a' -o -name '*.la' \) -exec rm -f {} \;

	useq character || rm -rf "${D}/${BASE_DIR}/{$(get_libdir)/yacs/mod_character*,include/yacs/character}"
	useq relationship || rm -rf	"${D}/${BASE_DIR}/{$(get_libdir)/yacs/mod_relationship*,include/yacs/relationship}"
	useq chat || rm -rf	"${D}/${BASE_DIR}/{$(get_libdir)/yacs/mod_chat*,include/yacs/chat}"
	useq news || rm -rf	"${D}/${BASE_DIR}/{$(get_libdir)/yacs/mod_news*,include/yacs/news}"
	useq examples || rm -rf	"${D}/${BASE_DIR}/{$(get_libdir)/yacs/mod_example*,include/yacs/example}"

	# multislot-dependant stuff
	local configdir piddir rcfile

	if useq multislot; then
		dodir /etc/yacs-${SLOT}
		configdir="/etc/yacs-${SLOT}"
		piddir="/var/run/yacs-${SLOT}"
		rcfile="yacsd-${SLOT}"

		keepdir /var/run/yacs-${SLOT}
		keepdir /var/log/yacs-${SLOT}
	else
		dodir /etc/yacs
		configdir="/etc/yacs"
		piddir="/var/run"
		rcfile="yacsd"

		dodir /var/run
		dodir /var/log
	fi

	# install rc files
	cp ${FILESDIR}/yacsd.rc-${PVR} yacsd.rc
	cp ${FILESDIR}/yacsd.rc.conf-${PVR} yacsd.confd

	for x in yacsd.rc yacsd.confd; do
		sed -i \
			-e "s,/usr/yacs/0.6,${BASE_DIR}," \
			-e "s,/var/run/yacs-0.6,${piddir}," \
			-e "s,/etc/yacs-0.6,${configdir}," \
			$x
	done

	newinitd yacsd.rc ${rcfile}
	newconfd yacsd.confd ${rcfile}

	# install yacsd config file
	local configfile="${D}/${configdir}/yacsd.conf"
	cp server/yacsd.conf-dist ${configfile}
	sed -i -e "s,/opt/surakware/lib,${BASE_DIR}/$(get_libdir)," ${configfile}

	# install common docs
	dodoc AUTHORS ChangeLog* NEWS README* TODO

	if use doc; then
		#ewarn "TODO: install man-pages and html version via doxygen"
		#dodoc -r doc/html
		true
	fi
}

# vim:ai:noet:ts=4:nowrap
