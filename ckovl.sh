#! /bin/sh
# ckovl.sh - checks overlay for manifest/digest changes

source /sbin/functions.sh

export PORTDIR_OVERLAY="${PORTDIR_OVERLAY:-/home/trapni/projects/gentoo/trapni/overlay}"

for CATEGORY in $(find . -mindepth 1 -maxdepth 1 -type d -name '*-*' | sed 's,./,,'); do
	pushd ${CATEGORY} &>/dev/null
	for PACKAGE in $(find . -mindepth 1 -maxdepth 1 -type d | sed 's,./,,' | grep -v .svn); do
		einfo " Checking ${CATEGORY}/${PACKAGE} ..."

		pushd ${PACKAGE} &>/dev/null
		for EBUILD in $(find . -maxdepth 1 -type f -name '*.ebuild' | sed 's,./,,'); do
			rm -f Manifest files/digest-*
			ebuild ${EBUILD} digest
		done
		popd &>/dev/null
	done
	popd &>/dev/null
done
