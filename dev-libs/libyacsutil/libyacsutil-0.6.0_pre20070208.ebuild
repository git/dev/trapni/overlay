# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit flag-o-matic

DESCRIPTION="YaCS utility library"
HOMEPAGE="http://battousai.mylair.de/yacs/"
SRC_URI="http://battousai.mylair.de/dist/yacs/yacs-${PV}.tar.bz2"
LICENSE="LGPL-2.1"
SLOT="0.6"
KEYWORDS="~amd64 ~x86"
IUSE="debug doc multislot"

RDEPEND=">=dev-libs/swl-0.5.0_pre20070201
		 >=net-libs/gnutls-1.4.4-r1
		 >=dev-db/mysql-5.0.26-r2"

DEPEND="${RDEPEND}
		doc? ( >=app-doc/doxygen-1.3.9.1 )"

BASE_DIR="${BASE_DIR:-/usr}"

S="${WORKDIR}/yacs-${PV}"

src_compile() {
	use debug && append-flags -O0 -g3
	use debug || append-flags -DNDEBUG=1

	useq multislot && [[ $BASE_DIR = "/usr" ]] && BASE_DIR="${BASE_DIR}/yacs/${SLOT}"

	einfo "Configuring for prefix ${BASE_DIR} ..."

#	./autogen.sh || die "autogen.sh failed"

	./configure \
		--prefix="${BASE_DIR}" \
		--host="${CHOST}" \
		--libdir="${BASE_DIR}/$(get_libdir)" \
		--with-libyacsutil \
		--disable-server \
		--without-libyacsclient \
		--without-mod_ychatstream \
		--without-mod_transform-plugins \
		|| die "./configure failed"

	cd support/yacsutil || die

	emake || die "make failed"

	if use doc; then
		#ewarn "TODO: generate docs {html,man} via doxygen"
		#make -C doc api-docs
		# XXX: install example/test files?
		true
	fi
}

src_install() {
	pushd support/yacsutil || die

	make install DESTDIR="${D}" || die

	dodoc AUTHORS ChangeLog* NEWS README* TODO

	if use doc; then
		#ewarn "TODO: install man-pages and html version via doxygen"
		#dodoc -r doc/html
		true
	fi
}

# vim:ai:noet:ts=4:nowrap
